package com.example.plugins.tutorial.jira.csvimport;

//import javax.inject.Inject;
import javax.servlet.*;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import javax.servlet.http.*;
import java.io.*;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import java.util.ArrayList;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.UpdateValidationResult;
import com.atlassian.jira.bc.issue.IssueService.IssueResult;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.config.util.JiraHome;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.NoSuchFileException;
import org.apache.log4j.*;
import com.atlassian.jira.exception.UpdateException;

public class importServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(importServlet.class);
    @ComponentImport
    private final TemplateRenderer templateRenderer;
   // @ComponentImport
    //private PageBuilderService pageBuilderService;
    JiraAuthenticationContext authenticationContext;
    PrintWriter out;
    private static final long serialVersionUID = 1L;
    IssueService issueService;
    IssueInputParameters issueInputParameters;
    IssueManager issueManager;
    MutableIssue issue;
    ApplicationUser user;
    CustomFieldManager cfm;
    String pathLog;
    BufferedWriter logFile;
    public importServlet(final TemplateRenderer templateRenderer) {
        this.templateRenderer = templateRenderer;
    }
    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
        this.authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        this.user = authenticationContext.getLoggedInUser();
        this.issueService = ComponentAccessor.getIssueService();
        this.issue = ComponentAccessor.getIssueFactory().getIssue();
        this.issueInputParameters = issueService.newIssueInputParameters();
        this.pathLog = ComponentAccessor.getComponentOfType(JiraHome.class).getLogDirectory().getAbsolutePath();
    }
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        out = resp.getWriter();
        final Map<String, Object> context = new HashMap<String, Object>();  
        final ArrayList<String> values= new ArrayList<String>();
        context.put("values",values);
        deleteAndCreateNewLogFile();
        //pageBuilderService.assembler().resources().requireWebResource("com.example.plugins.tutorial.jira.csvimport:importer-resources").requireContext("simple-csv-importer");
        resp.setContentType("text/html;charset=utf-8");
        templateRenderer.render("templates/csvImport.vm", context, resp.getWriter());  
    } 

    public void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        int numRows =0;
        final Map<String, Object> context = new HashMap<String, Object>(); 
        logFile = new BufferedWriter(new FileWriter(pathLog+"\\import-csv.log")); 
        writeInLog("*** Import CSV");
        log.error("*** Import CSV");
        if (ServletFileUpload.isMultipartContent(req)) {
            writeInLog("*** Import CSV: Starting import...");
            log.error("*** Import CSV: Starting import...");
            try {
                final List<FileItem> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(req);
                for (final FileItem item : multiparts) {
                    //
                    // If the item is a string content
                    //
                    if (!item.isFormField()) {
                        IssueService.IssueResult issueResult;  
                        final String content = item.getString();
                        final StringReader sReader = new StringReader(content);
                        //
                        // Reads the file with headers as ....
                        //
                        writeInLog("*** Import CSV: Reading input file...");
                        log.error("*** Import CSV: Reading input file...");
                        final Iterable<CSVRecord> records = CSVFormat.RFC4180.withDelimiter(';')
                                .withFirstRecordAsHeader().parse(sReader);
                        //
                        // Iterate over different rows
                        //
                        writeInLog("*** Import CSV: Processing input file...");
                        log.error("*** Import CSV: Processing input file...");
                        writeInLog("----------------------------------------------------------------------------"); 
                        for (final CSVRecord record : records) {
                            final String issuekey = record.get("Key");
                            final String summary = record.get("Nombre Jira");      
                            issueResult = issueService.getIssue(user, issuekey);
                            issue = issueResult.getIssue();
                            cfm=ComponentAccessor.getCustomFieldManager();    
                            writeInLog("*** Import CSV: Processing project:["+issuekey+"]"+summary);                                              
                            issueInputParameters.setSummary(summary);
                            writeInLog("*** Import CSV: Processing 'Previous years' fields group.");     
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Previous years IT Infrastructure").getId(), record.get("Previous years IT Infrastructure"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Previous years IT Development").getId(), record.get("Previous years IT Development"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Previous years Business").getId(), record.get("Previous years Business")); 
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Previous years Total").getId(), record.get("Previous years Total"));
                            writeInLog("*** Import CSV: Processing 'CY PAP' fields group.");   
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY PAP IT Infrastructure").getId(), record.get("CY PAP IT Infrastructure"));
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY PAP IT Development").getId(), record.get("CY PAP IT Development"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY PAP Business").getId(), record.get("CY PAP Business")); 
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY PAP Total").getId(), record.get("CY PAP Total"));
                            writeInLog("*** Import CSV: Processing 'CY REV1' fields group.");   
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY REV1 IT Infrastructure").getId(), record.get("CY REV1 IT Infrastructure"));
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY REV1 IT Development").getId(), record.get("CY REV1 IT Development"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY REV1 Business").getId(), record.get("CY REV1 Business")); 
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY REV1 Total").getId(), record.get("CY REV1 Total"));
                            writeInLog("*** Import CSV: Processing 'CY REV2' fields group.");   
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY REV2 IT Infrastructure").getId(), record.get("CY REV2 IT Infrastructure"));
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY REV2 IT Development").getId(), record.get("CY REV2 IT Development"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY REV2 Business").getId(), record.get("CY REV2 Business")); 
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY REV2 Total").getId(), record.get("CY REV2 Total"));                            
                            writeInLog("*** Import CSV: Processing 'CY EAC' fields group.");   
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY EAC IT Infrastructure").getId(), record.get("CY EAC IT Infrastructure"));
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY EAC IT Development").getId(), record.get("CY EAC IT Development"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY EAC Business").getId(), record.get("CY EAC Business")); 
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY EAC Total").getId(), record.get("CY EAC Total"));                            
                            writeInLog("*** Import CSV: Processing 'CY YTD' fields group.");   
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY YTD IT Infrastructure").getId(), record.get("CY YTD IT Infrastructure"));
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY YTD IT Development").getId(), record.get("CY YTD IT Development"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY YTD Business").getId(), record.get("CY YTD Business")); 
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY YTD Total").getId(), record.get("CY YTD Total"));                            
                            writeInLog("*** Import CSV: Processing 'CY+1 EAC' fields group.");   
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY+1 EAC IT Infrastructure").getId(), record.get("CY+1 EAC IT Infrastructure"));
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY+1 EAC IT Development").getId(), record.get("CY+1 EAC IT Development"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY+1 EAC Business").getId(), record.get("CY+1 EAC Business")); 
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("CY+1 EAC Total").getId(), record.get("CY+1 EAC Total"));                            
                            writeInLog("*** Import CSV: Processing 'Total Incurred' fields group.");   
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Total Incurred IT Infrastructure").getId(), record.get("Total Incurred IT Infrastructure"));
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Total Incurred IT Development").getId(), record.get("Total Incurred IT Development"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Total Incurred Business").getId(), record.get("Total Incurred Business")); 
                            writeInLog("*** Import CSV: Processing 'Total EAC' fields group.");   
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Total EAC IT Infrastructure").getId(), record.get("Total EAC IT Infrastructure"));
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Total EAC IT Development").getId(), record.get("Total EAC IT Development"));                                            
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Total EAC Business").getId(), record.get("Total EAC Business")); 
                            writeInLog("*** Import CSV: Processing 'Progress' fields group.");   
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Progress Actual (%)").getId(), record.get("Progress Actual (%)"));
                            issueInputParameters.addCustomFieldValue(cfm.getCustomFieldObjectByName("Progress Plan (%)").getId(), record.get("Progress Plan (%)")); 
                            writeInLog("*** Import CSV: Ready to update issue...");   
                            try{
                                updateIssue();
                                writeInLog("*** Import CSV. Project updated: ["+issuekey+"]"+summary);  
                                numRows++;
                                writeInLog("----------------------------------------------------------------------------"); 
                            } catch (final Exception ex) {
                                context.put("result","Import Failed."+ numRows +" rows have been processed");
                                context.put("resultType","error");
                                writeInLog("*** Import CSV: Import Failed due to " + ex +"."+ numRows +" rows have been processed");  
                                throw new IOException(ex);
                            }
                        }
                    }
                }
                // File uploaded successfully
                context.put("result","Import finalized Successfully. "+ numRows +" rows have been processed");
                context.put("resultType","success");
                writeInLog("*** Import CSV: Import finalized successfully."+ numRows +" rows have been processed");  
            } catch (final Exception ex) {
                context.put("result","Import Failed."+ numRows +" rows have been processed");
                context.put("resultType","error");
                writeInLog("*** Import CSV: Import Failed due to " + ex +"."+ numRows +" rows have been processed");  
            }
        } else {
            context.put("result","Only handled file upload request");
            context.put("resultType","error");
            writeInLog("*** Import CSV: Only handled file upload request");  
        }
		//
		// Redirects the response to VELOCITY TEMPLATE
        //
        logFile.close();
        resp.setContentType("text/html;charset=utf-8");
        templateRenderer.render("templates/csvUploadResult.vm", context, resp.getWriter());  
    }
    public MutableIssue updateIssue() throws UpdateException{
        IssueResult updateResult = new IssueResult(issue);
        final UpdateValidationResult updateValidationResult = issueService.validateUpdate(user, issue.getId() ,issueInputParameters);
        if (updateValidationResult.isValid()) {
            updateResult = issueService.update(user, updateValidationResult);
        }else {
            final Collection<String> errorMessages = updateValidationResult.getErrorCollection().getErrorMessages();
            for (final String errorMessage : errorMessages) {
                throw new UpdateException("*** Import CSV: Validation error:"+errorMessage+"/"+issue.getId());
            }
            final Map<String, String> errors = updateValidationResult.getErrorCollection().getErrors();
            final Set<String> errorKeys = errors.keySet();
            for (final String errorKey : errorKeys) {
                throw new UpdateException("*** Import CSV: Validation error key:"+errors.get(errorKey));                    
            }
        }
        if (!updateResult.isValid()) {
            final Collection<String> errorMessages = updateResult.getErrorCollection().getErrorMessages();
            for (final String errorMessage : errorMessages) {
                throw new UpdateException(("*** Import CSV: Update error:"+errorMessage));
            }
        }else issue = updateResult.getIssue();
    return issue;
    }
    public boolean deleteAndCreateNewLogFile() {
        try
        { 
            Files.deleteIfExists(Paths.get(pathLog+"\\import-csv.log")); 
        } 
        catch(NoSuchFileException e) 
        { 
            log.error("*** Import CSV: No such file/directory exists."); 
        } 
        catch(IOException e) 
        { 
            log.error("*** Import CSV: Invalid permissions to delete log file."); 
            return (false);
        } 
          
        log.error("*** Import CSV: Deletion successful."); 
        return (true);
    }
    public void writeInLog(String s){
        try{
            logFile.write(new Date().toString() + " - " + s);
            logFile.newLine();
        }
        catch(IOException e) 
        { 
            log.error("*** Import CSV: Error to write in log file."); 
        }
    }
}