package com.example.plugins.tutorial.jira.csvimport;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;

public class downloadLog extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
    }
    protected void doGet( HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

         response.setContentType("text/plain");

         response.setHeader("Content-disposition","attachment; filename=import-csv.log");
         String pathLog = ComponentAccessor.getComponentOfType(JiraHome.class).getLogDirectory().getAbsolutePath();
         File my_file = new File(pathLog+"\\import-csv.log");

         OutputStream out = response.getOutputStream();
         FileInputStream in = new FileInputStream(my_file);
         byte[] buffer = new byte[4096];
         int length;
         while ((length = in.read(buffer)) > 0){
            out.write(buffer, 0, length);
         }
         in.close();
         out.flush();
    }   
}