package com.example.plugins.tutorial.jira.csvimport;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;

public class importCondition extends AbstractWebCondition{
    @Override
    public boolean shouldDisplay(ApplicationUser user, JiraHelper helper) {
        ProjectManager projectManager = ComponentAccessor.getComponent(ProjectManager.class);
        ProjectActionSupport projectSupport = new ProjectActionSupport();
        Project currentProject = projectManager.getProjectObj(projectSupport.getSelectedProjectId());
        
        return currentProject.getKey().equals("REN1822");
    }

}